// Get easy access to server API
// use directly global: __MYSTATION_API__
// OR :
// const ServerAPI = __MYSTATION__.$APIManager.use('myStationAPI')

export default {
  namespaced: true,
  state: () => {
    return {
      // State here
    }
  },
  getters: {
    // Getters here
  },
  actions: {
    // Actions here
  },
  mutations: {
    // Mutations here
  }
}
