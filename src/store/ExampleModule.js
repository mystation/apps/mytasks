// Get easy access to server API
// use directly global: __MYSTATION_API__
// OR :
// const ServerAPI = __MYSTATION__.$APIManager.use('myStationAPI')

// Generate resource module mappers
const {
  todosState,
  todosGetters,
  todosActions,
  todosMutations
} = __MYSTATION__.generate.appModuleResourceMappers(__APP_NAME__, 'todos')

const {
  categoriesState,
  categoriesGetters,
  categoriesActions,
  categoriesMutations
} = __MYSTATION__.generate.appModuleResourceMappers(__APP_NAME__, 'categories')

export default {
  namespaced: true,
  state: () => {
    return {
      // Generate:
      // todos: [],
      // todosLoading: false,
      // todosCreating: false,
      // todosUpdating: false,
      // todosDeleting: false
      ...todosState,
      // Same for categories
      ...categoriesState,
      // Loaded indicator
      todosLoaded: false // True when todos and categories are loaded from server. False by default
    }
  },
  getters: {
    // Generate:
    // findTodos()
    ...todosGetters,
    // Same for categories
    ...categoriesGetters,
    /**
     * Retrieve todos by category
     * @param {number} categoryId - The category id
     * @return {Object[]}
     */
    findByCategory: (state) => (categoryId) => {
      return state.todos.filter((todo) => todo.category_id === categoryId)
    }
  },
  actions: {
    // Generate:
    // loadTodos()
    // createTodos()
    // updateTodos()
    // deleteTodos()
    ...todosActions,
    // Same for categories
    ...categoriesActions,
    /**
     * Init todos
     */
    initTodos: async function ({ state, dispatch, commit }) {
      await dispatch('loadTodos')
      await dispatch('loadCategories')
      commit('SET_TODOS_LOADED', true)
      return true
    }
  },
  mutations: {
    // Generate:
    // SET_TODOS()
    // SET_TODOS_LOADING()
    // SET_TODOS_CREATING()
    // SET_TODOS_UPDATING()
    // SET_TODOS_DELETING()
    ...todosMutations,
    // Same for categories
    ...categoriesMutations
  }
}
