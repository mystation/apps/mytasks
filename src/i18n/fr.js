export default {
  global: {
    category: 'catégorie',
    categories: 'catégories',
    todo: 'todo',
    todos: 'todos'
  },
  tabs: {
    myapp: 'myapp',
    options: 'options'
  }
}
