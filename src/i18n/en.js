export default {
  global: {
    category: 'category',
    categories: 'categories',
    todo: 'todo',
    todos: 'todos'
  },
  tabs: {
    myapp: 'myapp',
    options: 'options'
  }
}
