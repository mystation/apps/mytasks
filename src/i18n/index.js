// Import translations
import en from './en'
import fr from './fr'

// Make & export messages
export const messages = {
  en,
  fr
}

export default messages
