/*    Utils     */

export const sharedMixin = {
  computed: {
    // Add some globals to default
    // -> GLOBALS computed property is set by MyStation MainMixin to all vue instance
    // Example: <p>{{ GLOBALS.__APP_VERSION__ }}</p>
    // See vue.config.js webpack define options
    GLOBALS: function () {
      return {
        ...this.$root.ROOT_GLOBALS,
        __APP_NAME__: __APP_NAME__,
        __APP_VERSION__: __APP_VERSION__,
        __APP_COLOR__: __APP_COLOR__,
        __T_PREFIX__: __T_PREFIX__
      }
    }
  }
}

export const BaseComponent = __VUE__.extend({
  extends: __MYSTATION__.components.BaseAppComponent,
  appName: __APP_NAME__,
  mixins: [
    sharedMixin
  ]
})

export const createComponent = function (component) {
  return new BaseComponent(component)
}

export default {
  sharedMixin,
  createComponent,
  BaseComponent
}
