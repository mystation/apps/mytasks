// Main app component
import App from '@/App.vue'
// Translations
import i18n from '@/i18n'
// Pancakes
import MyAppPancake from '@/components/pancakes/MyAppPancake'
// Store modules
import MyAppModule from '@/store/MyAppModule'
// Styles
import '#styles/main.scss'

// Define app object
const app = {
  // Translations
  i18n: i18n,
  // Components
  components: {
    // Main app component
    app: App,
    // Pancakes (MyStation Dashboard)
    pancakes: {
      MyAppPancake: MyAppPancake
    }
  },
  // Shared data
  store: {
    modules: {
      MyAppModule: MyAppModule
    }
  },
  // App style files to import
  styles: {
    // Main app styles (built as dist/css/app.css)
    app: 'app.css'
  }
}

// You can access to MyStation everywhere using '__MYSTATION__'
__MYSTATION__.registerApp(__APP_NAME__, app)
