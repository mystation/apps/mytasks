/**
 * @hperchec/readme-generator Template EJS data example file
 */

'use strict'

// Dependencies
const fs = require('fs')
const path = require('path')
const markdownTable = require('markdown-table')

// Based on the package.json file, get some data and informations
const packageJson = require('../package.json')
// Get dependencies
const dependencies = packageJson.dependencies
// Get dev dependencies
const devDependencies = packageJson.devDependencies
// Homepage
const homepage = packageJson.homepage
// Repository URL
const repositoryUrl = packageJson.repository.url

// App config
const appConfig = require('../src/app.config.json')
// App translation langs
const translationLangs = fs.readdirSync(path.join(__dirname, '../src/i18n'))
// App icon
const appIconUrl = repositoryUrl.split('/').slice(0, (repositoryUrl.split('/').length - 1)).join('/') + '/' + appConfig.name + '/-/raw/master/public/app.icon.png'
// App color (without '#')
const appColor = appConfig.color.replace('#', '')

// Output a markdown formatted table from a js object
// Like:
// |name|version|
// |----|-------|
// |    |       |
function mdDependencies (deps) {
  return markdownTable([
    ['name', 'version'],
    ...(Object.entries(deps))
  ])
}

// Generate markdown list
function list (array, indent = 0) {
  let str = ''
  array.forEach(value => {
    const filename = value.split('.').shift()
    const lang = filename === 'index' ? null : filename
    if (lang) {
      str += ('- ' + lang + '\n').padStart(indent, ' ')
    }
  })
  return str
}

/**
 * Export data for readme file templating
 */
module.exports = {
  appIconUrl: appIconUrl,
  projectUrl: homepage,
  repositoryUrl: repositoryUrl,
  dependencies: mdDependencies(dependencies),
  appColor: appColor,
  appConfig: appConfig,
  translationsList: list(translationLangs)
}
