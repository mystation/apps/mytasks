const path = require('path')
const fs = require('fs')
const zipdir = require('zip-dir')

// Import app configuration
const appConfig = require('../src/app.config.json')

// 'dist' directory path
const distPath = path.resolve(__dirname, '../dist/')
// 'index.html' path
const indexPath = path.resolve(distPath, 'index.html')

// --

// Start
console.log('Post-build script: started...')

// Delete 'index.html' file
// ??? another solution? Don't need 'index.html' file.
// vue-cli error: disable 'html-webpack-plugin' with chainWebpack
console.log('Delete ' + indexPath)
fs.unlinkSync(indexPath)

// Zip all dist folder
const archiveName = appConfig.name + '.zip'
const archiveOutPath = path.resolve(distPath, archiveName)
console.log('Zip all folder ' + distPath + ' into : ' + archiveOutPath)
zipdir(distPath, { saveTo: archiveOutPath }, function (err, buffer) {
  // `buffer` is the buffer of the zipped file
  // And the buffer was saved to `~/myzip.zip`
  console.log('Archive created!')
})

// End
console.log('Post-build script: exit')
